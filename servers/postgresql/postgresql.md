# Starting PostgreSQL

When you need a new database, create a new directory on your `kea-dev` server.

Copy the `docker-compose.yml` file from this directory.
Make changes as necessary, e.g. database name, user name, user password, and network settings in the `docker-compose.yml` file.

Start the container from your new directory:

```bash
kea-db:~$ docker-compose up
```

# Connecting From The Server

To connect to the database shell from the server (logged in via SSH):

```bash
kea-db:~$ psql --host=127.0.0.1 --port=5432 --username=pguser pgdb
```

You will need to use the settings as specified in the `docker-compose.yml` file - these are the defaults.

# Connect From Your Local Machine

Create an SSH tunnel to the development server:

```bash
ssh -L 5432:kea-db:5432 kea-db
```

From a different terminal on your local machine:

```bash
psql --host=127.0.0.1 --port=5432 --username=pguser pgdb
```

You will need to use the settings as specified in the `docker-compose.yml` file - these are the defaults.

# Special Considerations

You will not need to create a database using the `createdb` command.
Docker will do this for you.
